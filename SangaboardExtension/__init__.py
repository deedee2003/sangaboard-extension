from labthings import current_action, fields, find_component, find_extension
from labthings.extensions import BaseExtension
from labthings.views import ActionView, PropertyView, View
from openflexure_microscope.stage.mock import MissingStage
from openflexure_microscope.api.utilities.gui import build_gui
import sangaboard
from sangaboard import Sangaboard
from labthings.utilities import path_relative_to
import logging
from urllib.request import urlopen, urlretrieve
from re import search
from semantic_version import Version
import os

class PwmFrequencyView(ActionView):
    args = {
        "value": fields.Int(required=True)
    }

    def get(self):
        extension: SangaboardExtension = find_extension("org.openflexure.sangaboard-extension")
        if not hasattr(extension.sangaboard, "illumination") or not extension.sangaboard.illumination.available:
            raise ValueError("Illumination module not found")
        return {"value": extension.sangaboard.illumination.pwm_frequency}

    def post(self, args):
        extension: SangaboardExtension = find_extension("org.openflexure.sangaboard-extension")
        if not hasattr(extension.sangaboard, "illumination") or not extension.sangaboard.illumination.available:
            raise ValueError("Illumination module not found")
        extension.sangaboard.illumination.pwm_frequency = int(args.get("value"))

class IlluminationStateView(ActionView):
    args = {
        "type": fields.String(),
        "channel": fields.Int(),
        "value": fields.Float(required=False)
    }

    def get(self):
        extension: SangaboardExtension = find_extension("org.openflexure.sangaboard-extension")
        if not extension.sangaboard.illumination.available:
            raise ValueError("Illumination module not found")
        illumination = extension.sangaboard.illumination
        response = {}
        response["pwm"] = illumination.get_pwm_led()
        response["cc"] = [illumination.cc_led]
        response["frequency"] = illumination.pwm_frequency
        response["channels"] = illumination.channels

        return response

    def post(self, args):
        extension: SangaboardExtension = find_extension("org.openflexure.sangaboard-extension")
        if not extension.sangaboard.illumination.available:
            raise ValueError("Illumination module not found")
        illumination = extension.sangaboard.illumination
        if args.get("type") == "cc":
            illumination.cc_led = args.get("value")

            return illumination.cc_led
        else:
            illumination.set_pwm_led(args.get("channel"), args.get("value"))

            return illumination.get_pwm_led(args.get("channel"))

class SystemUpgradeView(ActionView):
    args = {
        "channel": fields.String()
    }

    def get(self):
        extension: SangaboardExtension = find_extension("org.openflexure.sangaboard-extension")
        current_firmware = extension.sangaboard.parsed_firmware_version
        if current_firmware.prerelease:
            branch = current_firmware.prerelease[0]
            if branch == "dev":
                branch = "master"
        else:
            branch = "release"

        try:
            release_url = f"https://gitlab.com/filipayazi/sangaboard-firmware/-/raw/{branch}/src/config.h"
            config_h = urlopen(release_url).read()
            full_version = search('#define VERSION_STRING "(.*)"', config_h.decode())[1]
            version_string = search(r"Sangaboard Firmware v(.*)", full_version)[1]
            latest_firmware = Version.coerce(version_string)
            is_current = current_firmware >= latest_firmware

        except Exception as e:
            #can't get version data, possibly due to no internet connection
            version_string = None
            is_current = None
            logging.warn("Failed to get latest firmware version")
            logging.exception(e)

        return {
            "firmware": extension.sangaboard.firmware,
            "pysangaboard": sangaboard.__version__,
            "board": extension.sangaboard.board,
            "is_current": is_current,
            "latest_firmware": version_string,
            "branch": branch
        }

    def post(self, args):
        """
            Upgrade firmware on the board to latest in a specified branch
        """
        action = current_action()

        branch = args["channel"]

        action.data["step"] = "Preparing upgrade"

        firmware_elf_url = f"https://gitlab.com/filipayazi/sangaboard-firmware/-/jobs/artifacts/{branch}/raw/builds/firmware.elf?job=build"
        openocd_bin = os.path.expanduser("~/.platformio/packages/tool-openocd-rp2040-earlephilhower/bin/openocd")
        if not os.path.exists(openocd_bin):
            action.data["step"] = "Preparing required software"

            os.system('pio pkg install -g -t "earlephilhower/tool-openocd-rp2040-earlephilhower"')
            if not os.path.exists(openocd_bin):
                action.data["step"] = "openOCD installation failed"

                raise RuntimeError("Failed to install openocd")

        logging.info(f"Downloading: {firmware_elf_url} to /tmp/firmware.elf")
        action.data["step"] = "Downloading latest firmware"
        urlretrieve(firmware_elf_url, "/tmp/firmware.elf")
        #TODO check checksum?
        openocd_scripts_path = os.path.expanduser("~/.platformio/packages/tool-openocd-rp2040-earlephilhower/share/openocd/scripts")
        action.data["step"] = "Uploading firmware"

        if os.system(f"{openocd_bin} -d2 -s {openocd_scripts_path} -f interface/raspberrypi-swd.cfg -f target/rp2040.cfg -c 'adapter speed 1000' -c 'program {{/tmp/firmware.elf}}  verify reset; shutdown;'") != 0:
            action.data["step"] = "Upload failed"
            raise RuntimeError("Upload failed")

        action.data["step"] = "Reloading board"

        #replace sangaboard instance to reflect the changes
        extension: SangaboardExtension = find_extension("org.openflexure.sangaboard-extension")
        if extension.recreate_sangaboard():
            action.data["step"] = "Upgrade complete!"
        else:
            action.data["step"] = "Uploaded, please restart the microscope software."

# Create the extension class
class SangaboardExtension(BaseExtension):
    def __init__(self):
        super().__init__(
            "org.openflexure.sangaboard-extension",
            version="0.0.1",
            static_folder=path_relative_to(__file__, "static")
        )


        def gui_func():
            return {"icon": "developer_board", "frame": {"href": self.static_file_url("")}}

        self.add_view(IlluminationStateView, "/illumination")
        self.add_view(PwmFrequencyView, "/pwm-frequency")
        self.add_view(SystemUpgradeView, "/system")

        self.add_meta("gui", build_gui(gui_func, self))
        self._sangaboard: Sangaboard = None

    def recreate_sangaboard(self):
        microscope = find_component("org.openflexure.microscope")
        if hasattr(microscope.stage, "board") and not isinstance(microscope.stage, MissingStage):
            logging.info(f"Recreating sangaboard at {microscope.stage.board._ser.port}")
            microscope.stage.board = Sangaboard(microscope.stage.board._ser.port)
            self._sangaboard = microscope.stage.board

            return True
        return False


    @property
    def sangaboard(self) -> Sangaboard:
        if self._sangaboard is None:
            microscope = find_component("org.openflexure.microscope")
            self._sangaboard = microscope.stage.board
        return self._sangaboard
