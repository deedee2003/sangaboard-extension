# Sangaboard extension
This extension is aimed at users of Sangaboard v0.5. It can control illumination and upgrade the boards firmware using the SWD connection between the Pi and the sangaboard.

Support for the 4th motor is coming soon.

## pySangaboard dependency install
The extension requires an updated version of pySangaboard package, until the changes have been merged (hopefully soon) you will need to install it manually;
```
ofm activate
#in the ofm shell opened by above
git clone https://gitlab.com/filipayazi/pysangaboard.git
cd pysangaboard
git checkout sangaboardv5
pip uninstall sangaboard
python setup.py develop
```

## Install
To install, clone this repo into `/var/openflexure/extensions/microscope_extensions/`:
```
cd /var/openflexure/extensions/microscope_extensions/
git clone https://gitlab.com/filipayazi/sangaboard-extension.git
```
If you see some permissions errors, try
```
sudo chmod -R g+w /var/openflexure/
sudo adduser pi openflexure-ws
```
Log out and back in, and try again.

You will also need to install platformio using
```
ofm activate
#in the ofm shell opened by above
pip install -U platformio
```
You then need to restart OFM, you can use `ofm restart` to do that. The extension should then appear in the UI.
